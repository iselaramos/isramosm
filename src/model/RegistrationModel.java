package model;

import java.io.Serializable;

public class RegistrationModel implements Serializable{
    String PrimerNombre,SegundoNombre,PrimerApellido,SegundoApellido,FechaDeNacimiento,Ocupacion,FechaDeContratacion,AñosDeExperiencia;

    public String getPrimerNombre() {
        return PrimerNombre;
    }

    public void setPrimerNombre(String PrimerNombre) {
        this.PrimerNombre = PrimerNombre;
    }

    public String getSegundoNombre() {
        return SegundoNombre;
    }

    public void setSegundoNombre(String SegundoNombre) {
        this.SegundoNombre = SegundoNombre;
    }

    public String getPrimerApellido() {
        return PrimerApellido;
    }

    public void setPrimerApellido(String PrimerApellido) {
        this.PrimerApellido = PrimerApellido;
    }

    public String getSegundoApellido() {
        return SegundoApellido;
    }

    public void setSegundoApellido(String SegundoApellido) {
        this.SegundoApellido = SegundoApellido;
    }

    public String getFechaDeNacimiento() {
        return FechaDeNacimiento;
    }

    public void setFechaDeNacimiento(String FechaDeNacimiento) {
        this.FechaDeNacimiento = FechaDeNacimiento;
    }

    public String getOcupacion() {
        return Ocupacion;
    }

    public void setOcupacion(String Ocupacion) {
        this.Ocupacion = Ocupacion;
    }

    public String getFechaDeContratacion() {
        return FechaDeContratacion;
    }

    public void setFechaDeContratacion(String FechaDeContratacion) {
        this.FechaDeContratacion = FechaDeContratacion;
    }

    public String getAñosDeExperiencia() {
        return AñosDeExperiencia;
    }

    public void setAñosDeExperiencia(String AñosDeExperiencia) {
        this.AñosDeExperiencia = AñosDeExperiencia;
    }
    
    
}
