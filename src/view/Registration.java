package view;

import controller.RegistrationController;
import java.awt.Toolkit;
import javax.swing.JOptionPane;
import model.RegistrationModel;

public class Registration extends javax.swing.JInternalFrame {

    public Registration() {
        initComponents();
        setController();
    }
    
    public void setPersonData(RegistrationModel RM){
        PrimerNombreTextField.setText(RM.getPrimerNombre());
        SegundoNombreTextField.setText(RM.getSegundoNombre());
        PrimerApellidoTextField.setText(RM.getPrimerApellido());
        SegundoApellidoTextField.setText(RM.getSegundoApellido());
        FechaDeNacimientoTextField.setText(RM.getFechaDeNacimiento());
        OficioTextField.setText(RM.getOcupacion());
        FechaDeContratacionTextField.setText(RM.getFechaDeContratacion());
        AñosDeExperienciaTextField.setText(RM.getAñosDeExperiencia());
    }
    
    public RegistrationModel getPersonData(){
        RegistrationModel RM = new RegistrationModel();
        
        RM.setPrimerNombre(PrimerNombreTextField.getText());
        RM.setSegundoNombre(SegundoNombreTextField.getText());
        RM.setPrimerApellido(PrimerApellidoTextField.getText());
        RM.setSegundoApellido(SegundoApellidoTextField.getText());
        RM.setFechaDeNacimiento(FechaDeNacimientoTextField.getText());
        RM.setOcupacion(OficioTextField.getText());
        RM.setFechaDeContratacion(FechaDeContratacionTextField.getText());
        RM.setAñosDeExperiencia(AñosDeExperienciaTextField.getText());
        
        return RM;       
    }
    
    public void Clean(){
        PrimerNombreTextField.setText("");
        SegundoNombreTextField.setText("");
        PrimerApellidoTextField.setText("");
        SegundoApellidoTextField.setText("");
        FechaDeNacimientoTextField.setText("");
        OficioTextField.setText("");
        FechaDeContratacionTextField.setText("");
        AñosDeExperienciaTextField.setText("");
    }

    public void setController(){
        RegistrationController RC = new RegistrationController(this);
        this.saveButton.addActionListener(RC);
        this.SelectButton.addActionListener(RC);
        this.CleanButton.addActionListener(RC);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel9 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        PrimerNombreTextField = new javax.swing.JTextField();
        SegundoNombreTextField = new javax.swing.JTextField();
        PrimerApellidoTextField = new javax.swing.JTextField();
        OficioTextField = new javax.swing.JTextField();
        SegundoApellidoTextField = new javax.swing.JTextField();
        AñosDeExperienciaTextField = new javax.swing.JTextField();
        FechaDeContratacionTextField = new javax.swing.JFormattedTextField();
        FechaDeNacimientoTextField = new javax.swing.JFormattedTextField();
        saveButton = new javax.swing.JButton();
        CleanButton = new javax.swing.JButton();
        SelectButton = new javax.swing.JButton();

        jLabel9.setText("C$");

        setClosable(true);

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        jLabel12.setText("Primer Nombre");

        jLabel15.setText("Segundo Nombre");

        jLabel16.setText("Primer Apellido");

        jLabel17.setText("Segundo Apellido");

        jLabel18.setText("Fecha De Nacimiento");

        jLabel19.setText("Oficio");

        jLabel20.setText("Fecha De Contratacion");

        jLabel21.setText("Años de experiencia");

        PrimerApellidoTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PrimerApellidoTextFieldActionPerformed(evt);
            }
        });

        try {
            FechaDeContratacionTextField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        FechaDeContratacionTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        try {
            FechaDeNacimientoTextField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        FechaDeNacimientoTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        saveButton.setText("Save");

        CleanButton.setText("Clean");

        SelectButton.setText("Select");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel11)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17)
                    .addComponent(jLabel19)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel16)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(PrimerApellidoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel15)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(SegundoNombreTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel12)
                            .addGap(57, 57, 57)
                            .addComponent(PrimerNombreTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(OficioTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addGap(23, 23, 23)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(SegundoApellidoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(FechaDeNacimientoTextField)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(SelectButton)
                                .addComponent(jLabel21)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(AñosDeExperienciaTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(FechaDeContratacionTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(CleanButton)
                                .addGap(46, 46, 46)
                                .addComponent(saveButton)
                                .addGap(47, 47, 47)))))
                .addGap(51, 51, 51))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(PrimerNombreTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(SegundoNombreTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(PrimerApellidoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17)
                    .addComponent(SegundoApellidoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addGap(22, 22, 22))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(FechaDeNacimientoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(OficioTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(FechaDeContratacionTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(AñosDeExperienciaTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(saveButton)
                    .addComponent(CleanButton)
                    .addComponent(SelectButton))
                .addGap(4, 4, 4)
                .addComponent(jLabel11)
                .addContainerGap(10, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void PrimerApellidoTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PrimerApellidoTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PrimerApellidoTextFieldActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField AñosDeExperienciaTextField;
    private javax.swing.JButton CleanButton;
    private javax.swing.JFormattedTextField FechaDeContratacionTextField;
    private javax.swing.JFormattedTextField FechaDeNacimientoTextField;
    private javax.swing.JTextField OficioTextField;
    private javax.swing.JTextField PrimerApellidoTextField;
    private javax.swing.JTextField PrimerNombreTextField;
    private javax.swing.JTextField SegundoApellidoTextField;
    private javax.swing.JTextField SegundoNombreTextField;
    private javax.swing.JButton SelectButton;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JButton saveButton;
    // End of variables declaration//GEN-END:variables
}
