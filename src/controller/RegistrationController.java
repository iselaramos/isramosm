package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import model.RegistrationModel;
import view.Registration;

public class RegistrationController implements ActionListener{

    Registration R;
    RegistrationModel RM;
    JFileChooser d;
    
    public RegistrationController(Registration R){
        super();
        this.R = R;
        RM = new RegistrationModel();
        d = new JFileChooser();
    }

    public Registration getR() {
        return R;
    }

    public void setR(Registration R) {
        this.R = R;
    }

    public RegistrationModel getRM() {
        return RM;
    }

    public void setRM(RegistrationModel RM) {
        this.RM = RM;
    }

    public JFileChooser getD() {
        return d;
    }

    public void setD(JFileChooser d) {
        this.d = d;
    }
    
    
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "Save":
                d.showSaveDialog(R); 
                RM = R.getPersonData();
                writePerson(d.getSelectedFile());
                break;
            case "Select":
                d.showOpenDialog(R);  
                RM = readPerson(d.getSelectedFile());
                R.setPersonData(RM);
                break;
            case "Clean":
                R.Clean();
                break;
        }
    }
    
    private void writePerson(File file){
         try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file)); 
            w.writeObject(getRM());
            w.flush();
        } catch (IOException ex) {
            Logger.getLogger(RegistrationController.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
    
    private RegistrationModel readPerson(File file){
         try{
            ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(file));
            return (RegistrationModel)ois.readObject();
        }
        catch(FileNotFoundException e){
            JOptionPane.showMessageDialog(R, e.getMessage(),R.getTitle(),JOptionPane.WARNING_MESSAGE);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(RegistrationController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
